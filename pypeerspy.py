#!/usr/bin/python3
import socket
import time

def tracker_list_init():
    #fd = open("udp-trackers-ip-port.txt", 'r')
    fd = open("test-trackers.txt", 'r')
    tracker_list = []
    for line in fd:
        tracker_string = line.strip()
        tracker_data = tracker_string.split(":")
        ip = tracker_data[0]
        port = tracker_data[1]
        tracker = dict() 
        tracker['ip'] = ip
        tracker['port'] = port
        tracker_list.append(tracker)

    return tracker_list


def announce_data_init(connection_id, hash_info):
    data = b''
    hash_info_hex = bytes.fromhex(hash_info)

    announce_action = b'\x00\x00\x00\x01'
    transaction_id = b'\xF1\xE2\xD3\xC4'
    peer_id = b'AAAABBBBCCCCDDDDEEEE'
    downloaded = b'\x00\x00\x00\x00\x00\x00\x00\x00'
    left = b'\x00\x00\x00\x00\x00\x00\x00\x00'
    uploaded = b'\x00\x00\x00\x00\x00\x00\x00\x00'
    event = b'\x00\x00\x00\x00'
    ip_address = b'\x00\x00\x00\x00'
    key = b'\xDE\xAD\xBE\xEF'
    num_want = b'\x0F\xFF\xFF\xFF'
    port_num = b'\xD9\x03'

    data += connection_id
    data += announce_action
    data += transaction_id
    data += hash_info_hex
    data += peer_id
    data += downloaded
    data += left
    data += uploaded
    data += event
    data += ip_address
    data += key
    data += num_want
    data += port_num
    
    return data


def connection_data_init():
    protocol_id = b'\x00\x00\x04\x17\x27\x10\x19\x80'
    connection_action = b'\x00\x00\x00\x00'
    transaction_id = b'\xF1\xE2\xD3\xC4'
    data = protocol_id + connection_action + transaction_id
    return data

def tracker_sockets_init(tracker_list):
    tracker_sockets = []
    for tracker in tracker_list:
        ip = tracker['ip']
        port = int(tracker['port'])
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.setblocking(False)
        server_address = (ip, port)
        tracker_sockets.append((s, (ip,port)))

    return tracker_sockets


def tracker_connections_init(tracker_sockets, connect_time_threshold=3.0):
    # try to connect to as many trackers as possible within the connect_time_threshold
    pending_sockets = [] 
    bad_sockets = []
    for s in tracker_sockets:
        pending_sockets.append(s)
    for s in pending_sockets:
        con_data = connection_data_init()
        try:
            s[0].sendto(con_data, s[1])
        except:
            bad_sockets.append(s)
            pass
    for s in bad_sockets:
        pending_sockets.remove(s)

    connected_tracker_dict = dict()
    threshold = connect_time_threshold
    run_time = 0.0
    while run_time < threshold:
        time_start = time.perf_counter()
        for s in pending_sockets:
            try:
                recv_data = s[0].recv(256)
                connection_id = read_connection_id(recv_data)
                connected_tracker_dict[s] = connection_id
            except:
                pass
        for s in connected_tracker_dict:
            if s in pending_sockets:
                pending_sockets.remove(s)
        if len(pending_sockets) == 0:
            run_time += threshold
        time_end = time.perf_counter()
        run_time += time_end - time_start

    return connected_tracker_dict

def read_connection_id(data):
    # get the connection_id from the data 
    connection_id = data[8:16]
    return connection_id

def tracker_announces_init(connected_tracker_dict, hash_info):
    # the connected_tracker_dict is a dictionary with
    # a socket-to-connection_id mapping (d[(socket, addr)] = con_id)
    for tracker in connected_tracker_dict:
        # check connection id for error ... accounting for timeout
        connection_id = connected_tracker_dict[tracker]
        data = announce_data_init(connection_id, hash_info)
        tracker[0].sendto(data, tracker[1])
    
    return

def generate_peer_list(data):
    peer_data = data[20:]
    peer_data_size = int(len(peer_data) / 6)
    peer_list = []
    for i in range(peer_data_size):
        try:
            peer_ip = str(socket.inet_ntoa(peer_data[i*6: i*6 + 4]))
            peer_port = str(int.from_bytes(peer_data[i*6 + 4: i*6 + 6], "big"))
            peer = peer_ip + ":" + peer_port
            peer_list.append(peer)
        except:
            pass
    return peer_list

def tracker_peer_lists_init(connected_tracker_dict):
    global_peer_list = []
    poll = dict()
    poll_attempts = dict()
    for tracker in connected_tracker_dict:
        poll[tracker] = True
        poll_attempts[tracker] = 0
        #poll[tracker = NUMBER_HIGH
    
    threshold = 10000
    temp = []
    for tracker in poll:
        temp.append(tracker)

    while len(temp) > 0:
        for tracker in poll:
            #if tracker in temp and poll[tracker] < 1:
            if tracker in temp and poll[tracker] == False:
                temp.remove(tracker)
        for tracker in temp:
            try:
                data = tracker[0].recv(0xFFFF)
                peers = generate_peer_list(data)
                global_peer_list += peers
                poll[tracker] = False 
                #poll[tracker = 0
            except:
                poll_attempts[tracker] += 1
    
        for tracker in poll:
            if tracker in temp:
                if poll_attempts[tracker] > threshold:
                    poll[tracker] = False

    return global_peer_list

def tracker_sockets_close(tracker_sockets):
    for s in tracker_sockets:
        try:
            s.shutdown(socket.SHUT_RDWR)
            s.close()
        except:
            pass
    return

def query_trackers(hash_info, tracker_list):
    tracker_sockets = tracker_sockets_init(tracker_list)
    connected_tracker_dict = tracker_connections_init(tracker_sockets)
    tracker_announces_init(connected_tracker_dict, hash_info)
    global_peer_list = tracker_peer_lists_init(connected_tracker_dict)
    tracker_sockets_close(tracker_sockets)

    return global_peer_list

def create_hash_info_file():

    return

def hash_info_list_init(scrape_hash_info = False):
    hash_info_list = []
    if scrape_hash_info:
        create_hash_info_file()
    hash_info_file = open("info-hash.txt", 'r')
    for hash_info_line in hash_info_file:
        hash_info_list.append(hash_info_line.strip())

    return hash_info_list

def main():
    hash_info_list = hash_info_list_init(scrape_hash_info = False)
    tracker_list = tracker_list_init()
    global_peer_list = []

    for hash_info in hash_info_list:
        peer_list = query_trackers(hash_info, tracker_list)
        global_peer_list += peer_list
   
    # remove redundancy
    temp = set()
    for peer in global_peer_list:
        temp.add(peer)
    global_peer_list.clear()
    for peer in temp:
        global_peer_list.append(peer)

    for peer in global_peer_list:
        print(peer)

    return


if __name__ == "__main__":
    main()
